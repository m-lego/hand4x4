# 4x4 handwire helpers

these are just 4x4 helpers for handwire with rgb underglow.

the rgbs are sk6812/03 mini-e footprint, the rest of components are tht or smd (1206) footpritns.


spacing is the "standard" 19.05mm so be careful you check the spacking, for example original planck is 19.00mm

![front](pics/hand4x4-front.webp)

![front](pics/hand4x4-back.webp)
